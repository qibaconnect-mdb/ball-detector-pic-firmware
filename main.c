#include "mcc_generated_files/mcc.h"
#include "trigger_data.h"


uint16_t threshold = 50;
static uint16_t flag = 0;
Trigger_Data trigger;



void main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();
    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();
    LiDAR_Init();
    RS485_Init();
    LiDAR_SetStateStart();
    RS485_SetStateStart();
    
    trigger.distance=0;
    trigger.locked=false;
    trigger.time=0;
    
    while (1)
    {
        LiDAR_Task();
        trigger=LiDAR_DataAnalysis(threshold,trigger);
        trigger=RS485_Task(trigger);
    }
}
/**
 End of File
*/