#include "RS485.h"
#include "trigger_data.h"

#define MSG_FMT "_a%c%x%x%c\n"  //sprintf(buffer, MSG_FMT, cmd,dstL,dstH,tmL,tmH)
static _RS485_Control _rs485;
static bool started2 = false;
static bool read = false;
uint8_t buffer[10];
uint8_t period2 = 9; //1ms sampling period (clock is set to 1ms)
unsigned char football[]="---BallDetect v2.0---\r\n";
uint8_t CRC=0;
uint8_t i=0;
uint8_t f=0;

void RS485_Init(void){
    _rs485.stateMicro=_RS485_Iddle;
    _rs485.stateMacro=_RS485_Iddle;
    _rs485.timer=0;
}

Trigger_Data RS485_Task(Trigger_Data trigger){
    switch(_rs485.stateMicro){
        case _RS485_Iddle:
            if (_rs485.stateMacro==_RS485_Start){ //This way assures that the "Start confirmation" is just performed once (at first start))
                if (started2){
                    _rs485.stateMicro=_RS485_Read;
                }else{
                    _rs485.stateMicro=_RS485_Start;
                    started2=true;
                }     
            }else if (_rs485.stateMacro==_RS485_Write){
                _rs485.stateMicro=_RS485_Write;
            }else if (_rs485.stateMacro==_RS485_Read){
                _rs485.stateMicro=_RS485_Read;
            }else if (_rs485.stateMacro==_RS485_Stop){
                _rs485.stateMicro=_RS485_Stop;
            }
            break;
        case _RS485_Read:
            if(_rs485.timer > period2){
                if(returnindex()){
                    read = true;
                }
                _rs485.stateMicro=_RS485_Write;
                _rs485.timer=0;
            }
            break;
        case _RS485_Start:
            if(_rs485.timer > period2){
                LATCbits.LATC2 = 1;
                if(EUSART_is_tx_ready()){
                    puts_UART(football);
                }
                while(TXSTAbits.TRMT==0);
                LATCbits.LATC2 = 0;
                _rs485.stateMicro=_RS485_Iddle;
                _rs485.timer=0;  
            }
            break;
        case _RS485_Write: 
            if(_rs485.timer > period2){
                if (read){
                    switch(check_buffer()){
                        case 0: //failed verifications, or not endereced to this board
                            break;
                        case 1: //Broadcast reset
                            trigger.locked=false;
                            trigger.distance=0;
                            trigger.time=0;
                            resettime();
                            break;
                        case 2://Poll
                            LATCbits.LATC2 = 1;
                            if(EUSART_is_tx_ready()){
                                poll_answer(trigger);
                                send_RS485();
                            }
                            while(TXSTAbits.TRMT==0);
                            LATCbits.LATC2 = 0; 
                            break;
                        case 3://Reset this board
                            LATCbits.LATC2 = 1;
                            if(EUSART_is_tx_ready()){
                                reset_answer();
                                send_RS485();
                            }
                            while(TXSTAbits.TRMT==0);
                            LATCbits.LATC2 = 0; 
                            trigger.locked=false;
                            trigger.distance=0;
                            trigger.time=0;
                            resettime();
                            break;
                    }                   
                    clearbuffer();
                    read=false;
                    _rs485.stateMicro=_RS485_Iddle;
                    _rs485.timer=0;   
                }else{
                    _rs485.stateMicro=_RS485_Iddle;
                    _rs485.timer=0;   
                }
            }
            break;
        case _RS485_Stop:
            if (_rs485.timer > period2){
                _rs485.stateMicro=_RS485_Iddle;
                _rs485.timer=0;
            }
            break;
        default:
            break;     
        }
    return trigger;
    }

void RS485_Timer(void){
    if (_rs485.timer < 65535)
    {
        _rs485.timer++;
    }
}
void RS485_SetStateIddle(void){
    _rs485.stateMacro=_RS485_Iddle;
}

void RS485_SetStateStart(void){
    _rs485.stateMacro=_RS485_Start;
}

void RS485_SetStateWrite(void){
    _rs485.stateMacro=_RS485_Write;
}

void RS485_SetStateRead(void){
    _rs485.stateMacro=_RS485_Read;
}

void RS485_SetStateStop(void){
    _rs485.stateMacro=_RS485_Stop;
}
void puts_UART(const unsigned char *Str)
{
    while(*Str){
        EUSART_Write(*Str++);
    }
}
void send_RS485(void){
    for (f=0;f<9;f++){
        EUSART_Write(buffer[f]);
    }
}
void poll_answer(Trigger_Data trigger){
    memset(buffer,0,sizeof(buffer));
    buffer[0]=STARTBYTE;
    buffer[1]=PICADDRESS;
    buffer[2]=0x31;
    buffer[3]=(uint8_t)(trigger.distance & 0xff);
    buffer[4]=(uint8_t)(trigger.distance >> 8);
    buffer[5]=(uint8_t)(trigger.time & 0xff);
    buffer[6]=(uint8_t)(trigger.time >> 8);
    buffer[7]=crc_calculator(); 
    buffer[8]=ENDBYTE;
}

void reset_answer(void){
    memset(buffer,0,sizeof(buffer));
    buffer[0]=STARTBYTE;
    buffer[1]=PICADDRESS;
    buffer[2]=0x32;
    buffer[3]=0x00;
    buffer[4]=0x00;
    buffer[5]=0x00;
    buffer[6]=0x00;
    buffer[7]=crc_calculator();
    buffer[8]=ENDBYTE;
}
uint8_t crc_calculator (void){
    CRC=buffer[0];
    for (i=0;i<6;++i){
        CRC=buffer[i+1]^CRC;
    }
    return CRC;
}