#include "LiDAR.h"

#define lidar_add 0x50

static uint16_t time = 0;
uint8_t sent[3] = {1,2,7};
static uint8_t received[7];
static _LiDAR_Data _lidar;
static bool started = false;   //flag to garantee just one "star confirmation" per ignition 
uint16_t distance;
uint8_t period = 4;     //Task process period, ATTENTION, this is muliplied by the timer_IR programmed period!!!!
                        //period = 9 is equivalent to ((period+1)*1ms)=10ms task period 

void LiDAR_Init(void){
    _lidar.stateMicro=_LiDAR_Iddle;
    _lidar.stateMacro=_LiDAR_Iddle;
    _lidar.timer=0;
    _lidar.done=false;
    resettime();
}

void LiDAR_Task(void){
    switch(_lidar.stateMicro){
        case _LiDAR_Iddle:
            if (_lidar.stateMacro==_LiDAR_Start){ //This way assures that the "Start confirmation" is just performed once (at first start))
                if (started){
                    _lidar.stateMicro=_LiDAR_Write;
                }else{
                    _lidar.stateMicro=_LiDAR_Start;
                    started=true;
                }     
            }else if (_lidar.stateMacro==_LiDAR_Write){
                _lidar.stateMicro=_LiDAR_Write;
            }else if (_lidar.stateMacro==_LiDAR_Read){
                _lidar.stateMicro=_LiDAR_Read;
            }else if (_lidar.stateMacro==_LiDAR_Stop){
                _lidar.stateMicro=_LiDAR_Stop;
            }
            break;
        case _LiDAR_Start:
            if(_lidar.timer > period){
                if (I2C_Open(lidar_add)==0){  
                    if (I2C_Close()==0){ 
                        _lidar.stateMicro=_LiDAR_Write;
                        _lidar.timer=0;
                        //Verification made with success 
                    }else{
                        _lidar.stateMicro=_LiDAR_Start;
                        _lidar.timer=0;    
                    }  
                }else{
                        _lidar.stateMicro=_LiDAR_Start;
                        _lidar.timer=0;       
                    }   //trys again until the verification has success 
            }
            break;
        case _LiDAR_Write:
            if (_lidar.timer > period){
                while(!I2C_Open(lidar_add)); // sit here until we get the bus
                I2C_SetBuffer(sent,sizeof(sent));
                I2C_SetAddressNackCallback(NULL,NULL); //NACK polling?
                I2C_MasterWrite();
                while(I2C_BUSY == I2C_Close()); // sit here until finished.
                _lidar.stateMicro=_LiDAR_Read;
                _lidar.timer=0;
            }
            break;
        case _LiDAR_Read:
            if (_lidar.timer > period){
                while(!I2C_Open(lidar_add)); // sit here until we get the bus..
                I2C_SetBuffer(received,sizeof(received));
                I2C_MasterRead();
                while(I2C_BUSY == I2C_Close()); // sit here until finished.
                _lidar.done=true;
                _lidar.stateMicro=_LiDAR_Iddle;
                _lidar.timer=0;
            }
            break;
        case _LiDAR_Stop:
            if (_lidar.timer > period){
                _lidar.stateMicro=_LiDAR_Iddle;
                _lidar.timer=0;
            }
            break;
        default:
            break;    
    }  
}

void LiDAR_Timer(void){
    if (_lidar.timer < 65535)
    {
        _lidar.timer++;
    }
    if (time<65535){
        time++;
    }
}

void LiDAR_SetStateIddle(void){
    _lidar.stateMacro=_LiDAR_Iddle;
}

void LiDAR_SetStateStart(void){
    _lidar.stateMacro=_LiDAR_Start;
}

void LiDAR_SetStateWrite(void){
    _lidar.stateMacro=_LiDAR_Write;
}

void LiDAR_SetStateRead(void){
    _lidar.stateMacro=_LiDAR_Read;
}

void LiDAR_SetStateStop(void){
    _lidar.stateMacro=_LiDAR_Stop;
}
void resettime(void){
    time=0;
}

Trigger_Data LiDAR_DataAnalysis(uint16_t threshold, Trigger_Data trigger){
    if (_lidar.done){
        distance=(received[2]|(received[3]<<8));
        if (trigger.locked==false){
            if (((int)distance) < threshold){
                trigger.distance=distance;
                trigger.locked=true;
                trigger.time=time;
                LED_Toggle();
            }
        }
        _lidar.done=false;    
    }
    return trigger;
}


